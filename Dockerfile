FROM ubuntu
LABEL maintainer="Freem~ <freemanovec@protonmail.com>"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y \
	ffmpeg \
	build-essential \
	make \
	git
RUN git clone https://github.com/ccrisan/streameye.git
RUN cd streameye && make && make install
ENTRYPOINT [ "/entrypoint.sh" ]
EXPOSE 80
ENV SOURCE_URI="rtsp://127.0.0.1:554/live.sdp"
ADD entrypoint.sh /

